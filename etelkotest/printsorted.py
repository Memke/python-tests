import sys
import csv
import operator


def print_sorted():
    reader = csv.reader(open("dane.txt"), delimiter=" ")
    sortedlist = sorted(reader, key=operator.itemgetter(2))
    slicedlist = sortedlist[-40:-20]
    for line in slicedlist:
        print ' '.join(line)


if __name__ == "__main__":
    print_sorted()
